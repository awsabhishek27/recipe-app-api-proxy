# Recipe App Api Proxy Application

Nginx Proxy App for our recipe app API

### Environment variables

 * 'LISTEN_PORT' - Port to Listen on (default: '8000')
 * 'APP_HOST' - HostName of the app to forword request to (default: 'app')
 * 'APP_PORT' - Port of the app to forword request to (default: '9000')
 